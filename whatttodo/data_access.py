import os
import toolforge
import requests
from datetime import datetime, timedelta
import urllib.parse

def get_topics():
    if os.getenv('FLASK_ENV') == 'development':
        return [{'title': 'Physics', 'id': 1}, { 'title': 'Astronomy', 'id': 2}]
    conn = toolforge.connect('enwiki')
    wikiprojects = []
    with conn.cursor() as cur:
        cur.execute("select pap_project_id, pap_project_title from page_assessments_projects;")
        for row in cur.fetchall():
            wikiprojects.append({'id': row[0], 'title': row[1].decode('utf-8')})
    return wikiprojects

def get_languages():
    if os.getenv('FLASK_ENV') == 'development':
        return ['fa', 'az', 'de']
    conn = toolforge.connect('enwiki')
    langs = []
    with conn.cursor() as cur:
        cur.execute("select distinct ll_lang from langlinks;")
        for row in cur.fetchall():
            langs.append(row[0].decode('utf-8'))
    return langs

def get_missing_articles(lang, topic, importance):
    if os.getenv('FLASK_ENV') == 'development':
        return [
            {'title': 'Causality_(physics)', 'iw_count': 13, 'viewtitle': 'Causality (physics)' },
            {'title': 'Glossary_of_physics', 'iw_count': 8, 'viewtitle': 'Glossary of physics', },
        ]
    conn = toolforge.connect('enwiki')
    articles = []

    with conn.cursor() as cur:
        cur.execute("select pap_project_id from page_assessments_projects where pap_project_title = %s limit 1;", (topic,))
        project_id = cur.fetchone()[0]
        sql = "select page_title, count(*) as iw_count from page_assessments join page on pa_page_id = page_id join langlinks on ll_from = page_id where page_namespace = 0 and pa_project_id = " + str(int(project_id)) + " and pa_page_id not in (select ll_from from langlinks where ll_lang = %s) "
        values = (lang,)
        if importance != 'Any':
            sql += "and pa_importance = %s "
            values = (lang, importance)
        sql += "group by ll_from order by count(*) desc;"

        cur.execute(sql, values)
        for row in cur.fetchall():
            articles.append({
                'title': row[0].decode('utf-8'),
                'iw_count': row[1],
                'viewtitle': row[0].decode('utf-8').replace('_', ' ')
            })
    return articles


def get_short_aritcles(lang, topic, importance):
    if os.getenv('FLASK_ENV') == 'development':
        return [
            {'title': 'ﺎﺘﻣ', 'size': 500, 'll_count': 20, 'viewtitle': 'ﺎﺘﻣ' },
            {'title': 'ﺹﻮﺗ<200c>ﺸﻧﺎﺴﯾ', 'size': 1000, 'll_count': 30, 'viewtitle': 'ﺹﻮﺗ<200c>ﺸﻧﺎﺴﯾ' },
        ]

    conn = toolforge.connect('enwiki')
    articles_in_enwiki = []
    with conn.cursor() as cur:
        cur.execute("select pap_project_id from page_assessments_projects where pap_project_title = %s limit 1;", (topic,))
        project_id = cur.fetchone()[0]
        sql = "select ll_title from page_assessments join langlinks on pa_page_id = ll_from where pa_project_id = " + str(int(project_id)) + " and ll_lang = %s "
        values = (lang,)
        if importance != 'Any':
            sql += "and pa_importance = %s "
            values = (lang, importance)
        cur.execute(sql + ';', values)
        for row in cur.fetchall():
            articles_in_enwiki.append(row[0].decode('utf-8'))

    articles = []
    conn = toolforge.connect(lang + 'wiki')
    with conn.cursor() as cur:
        for batch in zip(*[iter(articles_in_enwiki)] * 100):
            batch_escape = "%s," * len(batch)
            cur.execute("select page_title, page_len, count(*) ll_count from page join langlinks on page_id = ll_from where page_namespace = 0 and page_title in (" + batch_escape[:-1] + ") and page_len < 15000 and page_is_redirect = 0 group by ll_from having count(*) > 1;", batch)
            for row in cur.fetchall():
                articles.append({
                    'title': row[0].decode('utf-8'),
                    'size': row[1],
                    'll_count': row[2],
                    'views': 0,
                    'viewtitle': row[0].decode('utf-8').replace('_', ' ')
                })

    return articles

    yesterday = datetime.now() - timedelta(10)
    a_month_ago = datetime.now() - timedelta(42)
    for i in range(len(articles)):
        article = articles[i]
        res = requests.get('https://wikimedia.org/api/rest_v1/metrics/pageviews/per-article/{}.wikipedia.org/all-access/user/{}/monthly/{}/{}'.format(
            urllib.parse.quote_plus(lang),
            urllib.parse.quote_plus(article['title']),
            datetime.strftime(a_month_ago, '%Y%m01'),
            datetime.strftime(yesterday, '%Y%m01')
        ), {'Accept': 'application/json', 'User-Agent': 'whattodo (https://whattodo.toolforge.org) - Amir Sarabadani (Ladsgroup@gmail.com)'})
        if 'items' in res.json():
            for case in res['items']:
                article['views'] += case.get('views', 0)

        articles[i] = article
    return articles
