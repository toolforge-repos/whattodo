from flask import Flask, render_template, request
import os
from whatttodo.data_access import get_topics, get_languages, get_missing_articles, get_short_aritcles

app = Flask(__name__)
dir = os.path.dirname(os.path.abspath(__file__))


@app.route("/")
def home():
    valid_topics = get_topics()
    valid_languages = get_languages()
    importances = ['Top', 'High', 'Mid', 'Low', 'Any']

    lang = request.args.get('lang')
    topic = request.args.get('topic')
    importance = request.args.get('importance')
    if lang in valid_languages and topic in [i['title'] for i in valid_topics] and importance in importances:
        return render_template(
            'result.html',
            lang=lang,
            topic=topic,
            importance=importance,
            importances=importances,
            valid_topics=valid_topics,
            valid_languages=valid_languages,
            missing_articles=get_missing_articles(lang, topic, importance))
    return render_template(
            'form.html',
            importances=importances,
            valid_topics=valid_topics,
            valid_languages=valid_languages)


@app.route("/whattoimprove")
def whattoimprove():
    valid_topics = get_topics()
    valid_languages = get_languages()
    importances = ['Top', 'High', 'Mid', 'Low', 'Any']

    lang = request.args.get('lang')
    topic = request.args.get('topic')
    importance = request.args.get('importance')
    if lang in valid_languages and topic in [i['title'] for i in valid_topics] and importance in importances:
        return render_template(
            'result_whattoimprove.html',
            lang=lang,
            topic=topic,
            importance=importance,
            importances=importances,
            valid_topics=valid_topics,
            valid_languages=valid_languages,
            short_articles=sorted(get_short_aritcles(lang, topic, importance), key=lambda a: a['ll_count'], reverse=True))
    return render_template(
            'form.html',
            path='whattoimprove',
            importances=importances,
            valid_topics=valid_topics,
            valid_languages=valid_languages)



if __name__ == "__main__":
    app.run(host='0.0.0.0')
